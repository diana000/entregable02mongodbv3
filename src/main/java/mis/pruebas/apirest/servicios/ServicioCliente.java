package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.CuentasCliente;
import org.springframework.data.domain.Page;

public interface ServicioCliente {

    public Page<Cliente> obtenerClientes(int pagina, int cantidad);
    public void insertarClienteNuevo(Cliente cliente);
    public Cliente obtenerCliente(String documento);
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);
    public void borrarCliente(String documento);
    public void agregarCuentaCliente(String documento, String numeroCuenta);
    public CuentasCliente obtenerCuentasCliente(String documento);
}
