package mis.pruebas.apirest.servicios.repositorios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.CuentasCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioClienteExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Query.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;


@Repository
public class RepositorioClienteExtendidoImpl implements RepositorioClienteExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharCliente(Cliente parche) {
        final Query query = query(where("_id").is(parche.documento));
        final Update update = new Update();

        set(update, "correo", parche.correo);
        set(update, "telefono", parche.telefono);
        set(update, "direccion", parche.direccion);
        set(update, "nombre", parche.nombre);
        set(update, "edad", parche.edad);
        set(update, "fechaNacimiento", parche.fechaNacimiento);

        mongoOperations.updateFirst(query, update, "cliente");
    }

    @Override
    public CuentasCliente obtenerCuentasCliente(String documento) {
        final var r =
                this.mongoOperations.aggregate(newAggregation(
                    /* 1 */ match(where("_id").is(documento)),
                    /* 2 */ lookup("cuenta","codigosCuentas","_id","cuentas"),
                    /* 3 */ addFields().addFieldWithValue("documento", "$_id").build(),
                    /* 4 */ project("documento", "nombre", "correo", "cuentas")
                 ), Cliente.class, CuentasCliente.class);
        final var cuentasClientes = r.getMappedResults();
        if (cuentasClientes.size() > 0)
            return cuentasClientes.get(0);
        // Si no hay resultados -> objeto vacio.
        final var vacio = new CuentasCliente();
        vacio.documento = documento;
        return vacio;
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }
}
